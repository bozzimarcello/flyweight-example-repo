﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "EnemyStats")]
public class EnemyStats : ScriptableObject
{
    public int maxHealthPoints;
    public int damagePoints;
    public float movementSpeed;
    public Ability ability;
}

[System.Serializable]
public class Ability
{
    public string mainAbility;
    public int mainAbilityBonus;
}
