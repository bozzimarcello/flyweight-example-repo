﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GamePrefs")]
public class GamePrefs : ScriptableObject
{
    private static GamePrefs instance;
    public static GamePrefs Instance
    {
        get
        {
            if (instance == null)
                instance = Resources.Load<GamePrefs>("GamePrefs");
            return instance;
        }
    }

    public Color hudTextColor;
    public Color hudBackgroundColor;
}
